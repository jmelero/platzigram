""" Users views"""
#Django
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

# Models
#from django.contrib.auth.models import User
from users.models import Profile
from django.contrib.auth.models import User
from posts.models import Post
# Forms

from users.forms import ProfileForm, SignUp


# Create your views here.


#Exceptions
from django.db.utils import IntegrityError
#Django
from django.views.generic import DetailView
from django.views.generic import ListView

class UserDetailView(LoginRequiredMixin, DetailView):

    template_name = 'users/detail.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'
    queryset = User.objects.all()
    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        """ Add user's posts to context. """
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        context['post'] = Post.objects.filter(user=user).order_by('-created')
        return context


def update_profile(request):
    """ uPDATE PROFILE"""
    #Cogemos los datos pasados y los cargamos a profile
    profile = request.user.profile
    form = request.user.profile


    if request.method == 'POST':
        #Pasamos el post y files (imágenes)
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            profile.website = data["website"]
            profile.biography = data["biography"]
            profile.picture = data["picture"]
            profile.phone_number = data["phone_number"]
            profile.save()
            url = reverse('users:detail', kwargs={'username': request.user.username})
            return redirect(url)
        else:
            #form = ProfileForm()
            form = ProfileForm(request.POST, request.FILES)



    return render(
        request=request,
        template_name='users/update_profile.html',
        context={
            'profile': profile,
            'user': request.user,
            'form': form
        }
    )




def login_view(request):
    """ Login view"""
    #import pdb; pdb.set_trace()
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('posts:feed')
        else:
            return render(request, 'users/login.html',{'error': 'Invalid username or password'})

    return render(request, 'users/login.html')



@login_required
def logout_view(request):
    logout(request)
    return redirect('users:login')



def signup_view(request):
    if request.method== 'POST':
        form = SignUp(request.POST)
        if form.is_valid():
            form.save()
            return redirect('users:login')
    else:
        form = SignUp()



    return render(
        request= request,
        template_name = 'users/signup.html',
        context = {
            'form': form
            }
        )
