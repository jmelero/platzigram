""" Posts views."""
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from datetime import datetime
# Create your views here.

from posts.forms import PostForm
from posts.models import Post


posts = Post.objects.all().order_by('-created')

class PostsFeedView(LoginRequiredMixin, ListView):
    """ Retuen all published posts."""
    template_name = 'posts/feed.html'
    model = Post
    ordering = ('-created',)
    paginate_by = 2
    context_object_name = 'posts'

@login_required
def create_post(request):
    """Create new post view"""
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            redirect('posts:feed')

    else:
        form = PostForm()

    return render(
        request = request,
        template_name = 'posts/new.html',
        context= {
            'form' : form,
            'user' : request.user,
            'profile': request.user.profile

        }
    )
