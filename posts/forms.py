""" Form for Post """

# Django
from django import forms


# Models
from posts.models import Post
from users.models import User

class PostForm(forms.ModelForm):
    """Post model form."""
    user2 = forms.ModelChoiceField(User.objects.all())
    posts = forms.ModelChoiceField(Post.objects.all())
    class Meta:
        """Form settings."""

        model = Post
        fields = ('user', 'profile', 'title', 'photo','user2','posts')
