""" Platzigram views """
#Django
from django.http import HttpResponse
from platzigram import views


# Utilities
from datetime import datetime

def hello_world(request):    
    return HttpResponse('Oh, hi! Current server time is {now}'.format(now=datetime.now().strftime('%b %dth, %Y - %H:%M hrs')))

def hi(request):
    """Hi."""          
    import json
    numbers = request.GET['numbers']    
    lista = [int(number) for number in numbers.split(',')]
    lista.sort()
    data = {
        'status': 'ok',
        'numbers': lista,
        'message': 'Sorted succesfull',
    }    
    lista2 = json.dumps(data)
    return HttpResponse(str(lista2),content_type='application/json')    